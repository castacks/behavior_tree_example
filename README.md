# Behavior Tree Example

## Demo

This package contains an example of using the behavior tree to perform a task. Below is a video showing what it should look like.

The robot starts at the Home position, takes off, and tries to fly to the Destination and return. If it goes through a No Fly Zone it will land and travel on the ground.

Click the image below to play the video:

[![Behavior Tree Example](http://img.youtube.com/vi/-xRF52uCHo8/0.jpg)](https://www.youtube.com/watch?v=-xRF52uCHo8 "Behavior Tree Example")


## Building the Code

To build the code do the following:

```
pip install graphviz
mkdir -p ws/src
cd ws/src/
git clone git@bitbucket.org:castacks/base_main_class.git
git clone git@bitbucket.org:castacks/behavior_tree.git
git clone git@bitbucket.org:castacks/behavior_tree_msgs.git
git clone git@bitbucket.org:castacks/behavior_tree_example.git
cd ..
catkin_make
```

## Running the Code

To run the code do:

```
cd ws/
source devel/setup.bash
mon launch behavior_tree_example behavior_tree_example.launch 
```

## Description

The behavior_tree package (https://bitbucket.org/castacks/behavior_tree/src/master/) contains a general overview on how to make a ROS node that interacts with the behavior tree. This package has specific examples of how to do so. You can either use only ROS std_msgs::String and std_msgs::Bool, or you can use the Action and Condtion helper classes from the behavior tree package. There are examples of both methods.

The configuration file that defines the behavior tree is in `config/example.tree`. It contains the conditions Visited Destination, At Home, On Ground, At Flight Altitude, On Ground, In No Fly Zone, and In Fly Zone. It contains the actions Takeoff, Land, Go Home, and Go To Destination.

The files travel.h and travel.cpp implement the At Home condition and Go Home action using only ROS std_msgs. They also implement the Visited Destination condition and Go To Destination using the Condition and Action helper classes.

The files takeoff_landing.h and takeoff_landing.cpp implement the At Flight Altitude condition and Takeoff actions using only ROS std_msgs. They also implement the On Ground condition and Land action using the Condition and Action helper classes.

The files no_fly_zone_condition.h and no_fly_zone_condition.cpp implement the In No Fly Zone condition using only ROS std_msgs and the In Fly Zone condition using the Condition helper class.


To get more familiar with behavior trees, you can try editing the configuration file so that the drone lands at the Destination and returns to home on the ground. The behavior should be as shown in the video below.

Click the image below to play the video:

[![Modified Behavior Tree Example](http://img.youtube.com/vi/c0Tw_QfP6rw/0.jpg)](https://www.youtube.com/watch?v=c0Tw_QfP6rw "Modified Behavior Tree Example")

## Additional Examples

### Example 2

This shows an example of using the Parallel Node and Not Decorator Node. This also has an example of an action node implemented in python. To run it do:

```
mon launch behavior_tree_example behavior_tree_example2.launch
```

### Example 3

This shows an example of using an `include` statement to setup a behavior tree with nested subtrees. The overall strucutre of the tree is the same as in Example 2.

```
mon launch behavior_tree_example behavior_tree_example3.launch
```

Author: John Keller
slack: kellerj
email: jkeller2@andrew.cmu.edu


### License ###
[This software is BSD licensed.](http://opensource.org/licenses/BSD-3-Clause)

Copyright (c) 2020, Carnegie Mellon University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


