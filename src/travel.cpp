#include <base/BaseNode.h>
#include <string>
#include "behavior_tree_example/travel.h"
#include <tf/transform_datatypes.h>

TravelNode::TravelNode(std::string node_name)
  : BaseNode(node_name){
}

bool TravelNode::initialize(){
  ros::NodeHandle* nh = get_node_handle();
  ros::NodeHandle* pnh = get_private_node_handle();

  
  // ********* CONDITION EXAMPLE WIHTOUT THE HELPER CLASS *********
  // Initialize data needed to handle the "At Home" condition using only ROS std_msgs.
  // The behavior tree contains a condition node called "At Home" so it will be expecting
  // a boolean published to a topic. The topic name is the condition node's label to lowercase,
  // with spaces replaced by underscores, and _success appended to the end, "/at_home_success".
  // The behavior tree has a "timeout" parameter. If a message is not recieved within the number
  // of seconds specified by "timeout", the condition will be considered in the failure state,
  // so you must publish it at a rate higher than 1/timeout Hz.
  at_home_pub = nh->advertise<std_msgs::Bool>("/at_home_success", 10);
  at_home = false;
  
  // ********* CONDITION EXAMPLE USING THE HELPER CLASS *********
  // Initialize data needed to handle the "Visited Destination" condition using the 
  // Condition helper class from the behavior_tree package.
  // Initialize the Condition object with the node handle, and the label on the Condition node
  // in the behavior tree, in this case "Visited Destination". The Condtion class automatically
  // sets up a publisher to the /visited_destination_success topic. You can set the Condition class
  // to success by calling visited_destination_condition->set(true) or failure by calling
  // visited_destination_condition->set(false). You can query the condition's bool using
  // visited_destination_condition->get(). Publish the condition by calling visited_destination_condition->publish().
  // Examples of these function calls are in the execute() function.
  // As in the method above using only ROS std msgs, you must call the Condition variable's publish
  // function fast enough so that the behavior tree doesn't consider it timed out.
  visited_destination_condition = new bt::Condition("Visited Destination");
  
  
  // ********* ACTION EXAMPLE WIHTOUT THE HELPER CLASS *********
  // Initialize data needed to handle the "Go Home" action  by directly publishing and subscribing to topics.
  // We need to subscribe to a std_msgs::Bool topic that tells us whether or not
  // the action is active. Since the label on the actio node in the behavior tree is "Go Home",
  // the behavior tree will publish the Bool message to the "/go_home_active" topic.
  // We also need to publish a std_msgs::String topic that
  // tells the bevahior tree the status of the action while it is active.
  // Nothing needs to be published while the action is not active.
  // The string should be "SUCCESS", "RUNNING", or "FAILURE". The behavior tree expects
  // the String status message to be published to the "/go_home_status" topic.
  // As in the condition publisher, we need to make sure the status gets published
  // at a high enough rate so that it isn't considered a timeout by the behavior tree.
  go_home_active_sub = nh->subscribe("/go_home_active", 10,
				     &TravelNode::go_home_active_callback, this);
  go_home_active = false;
  go_home_status_pub = nh->advertise<behavior_tree_msgs::Status>("/go_home_status", 10);
  
  // ********* ACTION EXAMPLE USING THE HELPER CLASS *********
  // Initialize data needed to handle the "Go To Destination" action using the 
  // Action helper class from the behavior_tree package.
  // The action class needs a node handle as the first parameter and
  // the label of the action node in the behavior tree's config file, in this case "Go To Destination".
  go_to_destination_action = new bt::Action("Go To Destination");
  
  
  // init subscribers
  robot_odom_sub = nh->subscribe("/odometry", 10, &TravelNode::robot_odom_callback, this);
  home_sub = nh->subscribe("/home", 10, &TravelNode::home_callback, this);
  destination_sub = nh->subscribe("/destination", 10, &TravelNode::destination_callback, this);
  
  // init publishers
  vel_cmd_pub = nh->advertise<geometry_msgs::TwistStamped>("/vel_cmd", 10);
  
  // initialization
  got_robot_odom = false;
  got_home = false;
  got_destination = false;
  acceptance_radius = 0.01;
  
  
  return true;
}


bool TravelNode::execute(){
  // wait for data
  if(!got_robot_odom || !got_home || !got_destination)
    return true;
  
  // ********* AT HOME CONDITION EXAMPLE USING ROS STD_MSGS *********
  // The at home condition should be set to success (true) if the robot is within
  // a threshold distance of the home location and failure (false) otherwise.
  float home_distance = distance(robot_odom, home);
  at_home = home_distance < acceptance_radius;
  // Publish the condition as a std_msgs::Bool.
  std_msgs::Bool at_home_msg;
  at_home_msg.data = at_home;
  at_home_pub.publish(at_home_msg);
  
  // ********* VISITED DESTINATION CONDITION EXAMPLE USING THE HELPER CLASS *********
  // The visited destination condition should be set to success (true) when we first
  // get close enough to the destination point and remain true forever.
  float destination_distance = distance(robot_odom, destination);
  if(!visited_destination_condition->get()) // Only changed the condition value if it hasn't been set to true yet.
    visited_destination_condition->set(destination_distance < acceptance_radius);
  visited_destination_condition->publish(); // Publish the condition.
  
  // ********* GO HOME ACTION EXAMPLE WITHOUT THE HELPER CLASS *********
  if(go_home_active){ // Make sure the robot tries to go home when the go home action is active.
    behavior_tree_msgs::Status go_home_status;
    geometry_msgs::TwistStamped vel_cmd;
    vel_cmd.header.stamp = ros::Time::now();
    vel_cmd.header.frame_id = "world";
    
    if(home_distance < acceptance_radius){
      // Set the status to "SUCCESS" if the robot has reached the home location
      go_home_status.status = behavior_tree_msgs::Status::SUCCESS;
      // Command zero velocity when the robot is done traveling home.
      vel_cmd.twist.linear.x = vel_cmd.twist.linear.y = vel_cmd.twist.linear.z = 0;
    }
    else{
      // Set the status to "RUNNING" while the robot is moving toward the home location.
      go_home_status.status = behavior_tree_msgs::Status::RUNNING;
      // Command a velocity towards the home location.
      tf::Vector3 vel(home.point.x - robot_odom.pose.pose.position.x,
		      home.point.y - robot_odom.pose.pose.position.y, 0);
      vel = 0.3*vel.normalized();
      vel_cmd.twist.linear.x = vel.x();
      vel_cmd.twist.linear.y = vel.y();
      vel_cmd.twist.linear.z = vel.z();
    }
      
    vel_cmd_pub.publish(vel_cmd);

    // Publish the go home action's status as a behavior_tree_msgs::Status.
    go_home_status_pub.publish(go_home_status);
  }
  
  // ********* GO TO DESTINATION ACTION EXAMPLE USING THE HELPER CLASS *********
  // Make sure the robot only tries to go to the destination while the go to destination action is active
  if(go_to_destination_action->is_active()){ 
    geometry_msgs::TwistStamped vel_cmd;
    vel_cmd.header.stamp = ros::Time::now();
    vel_cmd.header.frame_id = "world";
    
    if(destination_distance < acceptance_radius){
      // Set the status to success if the robot has reached the destination location.
      go_to_destination_action->set_success();
      // Command zero velocity when the robot is done traveling to the destination.
      vel_cmd.twist.linear.x = vel_cmd.twist.linear.y = vel_cmd.twist.linear.z = 0;
    }
    else{
      // Set the status to running while the robot is moving towards the destination location.
      go_to_destination_action->set_running();
      // Command a velocity towards the destination location.
      tf::Vector3 vel(destination.point.x - robot_odom.pose.pose.position.x,
		      destination.point.y - robot_odom.pose.pose.position.y, 0);
      vel = 0.3*vel.normalized();
      vel_cmd.twist.linear.x = vel.x();
      vel_cmd.twist.linear.y = vel.y();
      vel_cmd.twist.linear.z = vel.z();
    }
      
    vel_cmd_pub.publish(vel_cmd);

    // Publish the go to destination action's status.
    go_to_destination_action->publish();
  }
    
  return true;
}


void TravelNode::go_home_active_callback(std_msgs::Bool msg){
  go_home_active = msg.data;
}

float TravelNode::distance(nav_msgs::Odometry odom, geometry_msgs::PointStamped point){
  float distance = sqrt(pow(odom.pose.pose.position.x - point.point.x, 2) +
			pow(odom.pose.pose.position.y - point.point.y, 2));
  return distance;
}

void TravelNode::robot_odom_callback(nav_msgs::Odometry robot_odom){
  got_robot_odom = true;
  this->robot_odom = robot_odom;
}

void TravelNode::home_callback(geometry_msgs::PointStamped home){
  got_home = true;
  this->home = home;
}

void TravelNode::destination_callback(geometry_msgs::PointStamped destination){
  got_destination = true;
  this->destination = destination;
}

TravelNode::~TravelNode(){
}

BaseNode* BaseNode::get(){
  TravelNode* node = new TravelNode("TravelNode");
  return node;
}
