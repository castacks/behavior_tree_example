#include <base/BaseNode.h>
#include <string>
#include "behavior_tree_example/robot_node.h"
#include <tf/transform_datatypes.h>

RobotNode::RobotNode(std::string node_name)
  : BaseNode(node_name){
}

bool RobotNode::initialize(){
  ros::NodeHandle* nh = get_node_handle();
  ros::NodeHandle* pnh = get_private_node_handle();
  
  // init subscribers
  vel_cmd_sub = nh->subscribe("/vel_cmd", 10, &RobotNode::vel_cmd_callback, this);
  ang_vel_cmd_sub = nh->subscribe("/ang_vel_cmd", 10, &RobotNode::ang_vel_cmd_callback, this);

  // init publishers
  robot_odom_pub = nh->advertise<nav_msgs::Odometry>("/odometry", 10);
  home_pub = nh->advertise<geometry_msgs::PointStamped>("/home", 10);
  destination_pub = nh->advertise<geometry_msgs::PointStamped>("/destination", 10);
  no_fly_zone_pub = nh->advertise<geometry_msgs::PointStamped>("/no_fly_zone", 10);
  no_fly_zone_radius_pub = nh->advertise<std_msgs::Float32>("/no_fly_zone_radius", 10);
  no_fly_zone_vis_pub = nh->advertise<visualization_msgs::MarkerArray>("/no_fly_zone_vis", 10);
  

  // initialization
  home.header.frame_id = "world";
  home.point.x = 0;
  home.point.y = 0;
  home.point.z = 0;

  destination.header.frame_id = "world";
  destination.point.x = 10;
  destination.point.y = 0;
  destination.point.z = 0;

  no_fly_zone.header.frame_id = "world";
  no_fly_zone.point.x = 5;
  no_fly_zone.point.y = 0;
  no_fly_zone.point.z = 0;

  no_fly_zone_radius.data = 2.f;

  float marker_height = 10;
  no_fly_zone_marker.header.frame_id = "world";
  no_fly_zone_marker.ns = "no_fly_zone";
  no_fly_zone_marker.id = 0;
  no_fly_zone_marker.type = visualization_msgs::Marker::CYLINDER;
  no_fly_zone_marker.action = visualization_msgs::Marker::ADD;
  no_fly_zone_marker.pose.orientation.w = 1;
  no_fly_zone_marker.pose.position.x = no_fly_zone.point.x;
  no_fly_zone_marker.pose.position.y = no_fly_zone.point.y;
  no_fly_zone_marker.pose.position.z = marker_height/2;
  no_fly_zone_marker.scale.x = 2*no_fly_zone_radius.data;
  no_fly_zone_marker.scale.y = 2*no_fly_zone_radius.data;
  no_fly_zone_marker.scale.z = marker_height;
  no_fly_zone_marker.color.r = 1;
  no_fly_zone_marker.color.g = 0;
  no_fly_zone_marker.color.b = 0;
  no_fly_zone_marker.color.a = 0.3;
  
  no_fly_zone_label.header.frame_id = "world";
  no_fly_zone_label.ns = "no_fly_zone_label";
  no_fly_zone_label.id = 0;
  no_fly_zone_label.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
  no_fly_zone_label.action = visualization_msgs::Marker::ADD;
  no_fly_zone_label.pose.orientation.w = 1;
  no_fly_zone_label.pose.position.x = no_fly_zone.point.x;
  no_fly_zone_label.pose.position.y = no_fly_zone.point.y;
  no_fly_zone_label.pose.position.z = marker_height/2;
  no_fly_zone_label.scale.x = 1;//2*no_fly_zone_radius.data;
  no_fly_zone_label.scale.y = 1;//2*no_fly_zone_radius.data;
  no_fly_zone_label.scale.z = 1;//marker_height;
  no_fly_zone_label.color.r = 1;
  no_fly_zone_label.color.g = 1;
  no_fly_zone_label.color.b = 1;
  no_fly_zone_label.color.a = 1;
  no_fly_zone_label.text = "No Fly Zone";

  home_label.header.frame_id = "world";
  home_label.ns = "home_label";
  home_label.id = 0;
  home_label.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
  home_label.action = visualization_msgs::Marker::ADD;
  home_label.pose.orientation.w = 1;
  home_label.pose.position.x = home.point.x;
  home_label.pose.position.y = home.point.y;
  home_label.pose.position.z = home.point.z;
  home_label.scale.x = 0.5;
  home_label.scale.y = 0.5;
  home_label.scale.z = 0.5;
  home_label.color.r = 1;
  home_label.color.g = 1;
  home_label.color.b = 1;
  home_label.color.a = 1;
  home_label.text = "Home";
  
  destination_label.header.frame_id = "world";
  destination_label.ns = "destination_label";
  destination_label.id = 0;
  destination_label.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
  destination_label.action = visualization_msgs::Marker::ADD;
  destination_label.pose.orientation.w = 1;
  destination_label.pose.position.x = destination.point.x;
  destination_label.pose.position.y = destination.point.y;
  destination_label.pose.position.z = destination.point.z;
  destination_label.scale.x = 0.5;
  destination_label.scale.y = 0.5;
  destination_label.scale.z = 0.5;
  destination_label.color.r = 1;
  destination_label.color.g = 1;
  destination_label.color.b = 1;
  destination_label.color.a = 1;
  destination_label.text = "Destination";

  marker_array.markers.push_back(no_fly_zone_marker);
  marker_array.markers.push_back(no_fly_zone_label);
  marker_array.markers.push_back(home_label);
  marker_array.markers.push_back(destination_label);
  
  odom.header.frame_id = "world";
  odom.child_frame_id = "world";
  odom.pose.pose.orientation.w = 1;
  
  last_execute_time = ros::Time::now();
  
  return true;
}

bool RobotNode::execute(){
  // keep track of time
  ros::Time now = ros::Time::now();
  double dt = (now - last_execute_time).toSec();
  last_execute_time = now;

  // update the robot's position based on the velocity command
  odom.pose.pose.position.x += vel_cmd.twist.linear.x*dt;
  odom.pose.pose.position.y += vel_cmd.twist.linear.y*dt;
  odom.pose.pose.position.z += vel_cmd.twist.linear.z*dt;

  
  // update the robot's orientation based on the velocity command
  double roll, pitch, yaw;
  tf::Quaternion q(odom.pose.pose.orientation.x, odom.pose.pose.orientation.y, odom.pose.pose.orientation.z, odom.pose.pose.orientation.w);
  tf::Matrix3x3 m(q);
  m.getRPY(roll, pitch, yaw);
  roll += ang_vel_cmd.twist.angular.x*dt;
  pitch += ang_vel_cmd.twist.angular.y*dt;
  yaw += ang_vel_cmd.twist.angular.z*dt;
  q.setRPY(roll, pitch, yaw);
  odom.pose.pose.orientation.x = q.x();
  odom.pose.pose.orientation.y = q.y();
  odom.pose.pose.orientation.z = q.z();
  odom.pose.pose.orientation.w = q.w();

  // publish
  odom.header.stamp = home.header.stamp = destination.header.stamp = no_fly_zone.header.stamp = no_fly_zone_marker.header.stamp = ros::Time::now();
  robot_odom_pub.publish(odom);
  home_pub.publish(home);
  destination_pub.publish(destination);
  no_fly_zone_pub.publish(no_fly_zone);
  no_fly_zone_radius_pub.publish(no_fly_zone_radius);
  no_fly_zone_vis_pub.publish(marker_array);//no_fly_zone_marker);
  
  return true;
}

void RobotNode::vel_cmd_callback(geometry_msgs::TwistStamped twist){
  vel_cmd = twist;
}

void RobotNode::ang_vel_cmd_callback(geometry_msgs::TwistStamped twist){
  ang_vel_cmd = twist;
}

RobotNode::~RobotNode(){
}

BaseNode* BaseNode::get(){
  RobotNode* robot_node = new RobotNode("RobotNode");
  return robot_node;
}
