#include <base/BaseNode.h>
#include <string>
#include "behavior_tree_example/no_fly_zone_condition.h"


NoFlyZoneConditionNode::NoFlyZoneConditionNode(std::string node_name)
  : BaseNode(node_name){
  
}

bool NoFlyZoneConditionNode::initialize(){
  ros::NodeHandle* nh = get_node_handle();
  ros::NodeHandle* pnh = get_private_node_handle();

  // ********* CONDITION EXAMPLE WIHTOUT THE HELPER CLASS *********
  // Initialize data needed to handle the "In No Fly Zone" condition using only ROS std_msgs.
  // The behavior tree contains a condition node called "In No Fly Zone" so it will be expecting
  // a boolean published to a topic. The topic name is the condition node's label to lowercase,
  // with spaces replaced by underscores, and _success appended to the end, "/in_no_fly_zone_success".
  // The behavior tree has a "timeout" parameter. If a message is not recieved within the number
  // of seconds specified by "timeout", the condition will be considered in the failure state,
  // so you must publish it at a rate higher than 1/timeout Hz.
  in_no_fly_zone_pub = nh->advertise<std_msgs::Bool>("/in_no_fly_zone_success", 10);

    // ********* CONDITION EXAMPLE USING THE HELPER CLASS *********
  // Initialize data needed to handle the "In Fly Zone" condition using the 
  // Condition helper class from the behavior_tree package.
  // Initialize the Condition object with the node handle, and the label on the Condition node
  // in the behavior tree, in this case "In Fly Zone". The Condtion class automatically
  // sets up a publisher to the /in_fly_zone_success topic. You can set the Condition class
  // to success by calling in_fly_zone_condition->set(true) or failure by calling
  // in_fly_zone_condition->set(false). You can query the condition's bool using
  // in_fly_zone_condition->get(). Publish the condition by calling in_fly_zone_condition->publish().
  // Examples of these function calls are in the execute() function.
  // As in the method above using only ROS std msgs, you must call the Condition variable's publish
  // function fast enough so that the behavior tree doesn't consider it timed out.
  in_fly_zone_condition = new bt::Condition("In Fly Zone");
  
  
  // init subscribers
  robot_odom_sub = nh->subscribe("/odometry", 10,
				 &NoFlyZoneConditionNode::robot_odom_callback, this);
  no_fly_zone_sub = nh->subscribe("/no_fly_zone", 10,
				  &NoFlyZoneConditionNode::no_fly_zone_callback, this);
  no_fly_zone_radius_sub = nh->subscribe("/no_fly_zone_radius", 10,
					 &NoFlyZoneConditionNode::no_fly_zone_radius_callback,
					 this);
  
  // init publishers
  in_no_fly_zone_pub = nh->advertise<std_msgs::Bool>("/in_no_fly_zone_success", 10);
  
  // initialization
  got_robot_odom = false;
  got_no_fly_zone = false;
  got_no_fly_zone_radius = false;
  
  return true;
}

bool NoFlyZoneConditionNode::execute(){
  if(got_robot_odom && got_no_fly_zone && got_no_fly_zone_radius){
    float distance = sqrt(pow(robot_odom.pose.pose.position.x - no_fly_zone.point.x, 2) +
			  pow(robot_odom.pose.pose.position.y - no_fly_zone.point.y, 2));

    // ********* IN NO FLY ZONE CONDITION EXAMPLE USING ROS STD_MSGS *********
    // Check if the robot is with the radius of the no fly zone point.
    bool in_no_fly_zone = distance <= no_fly_zone_radius;
    // Publish a bool indicating success or failure.
    std_msgs::Bool in_no_fly_zone_msg;
    in_no_fly_zone_msg.data = in_no_fly_zone;
    in_no_fly_zone_pub.publish(in_no_fly_zone_msg);

    // ********* IN FLY ZONE CONDITION EXAMPLE USING THE HELPER CLASS *********
    in_fly_zone_condition->set(!in_no_fly_zone); // Set the condition.
    in_fly_zone_condition->publish();  // Publish.
  }
  
  return true;
}


void NoFlyZoneConditionNode::robot_odom_callback(nav_msgs::Odometry robot_odom){
  got_robot_odom = true;
  this->robot_odom = robot_odom;
}

void NoFlyZoneConditionNode::no_fly_zone_callback(geometry_msgs::PointStamped no_fly_zone){
  got_no_fly_zone = true;
  this->no_fly_zone = no_fly_zone;
}

void NoFlyZoneConditionNode::no_fly_zone_radius_callback(std_msgs::Float32 no_fly_zone_radius){
  got_no_fly_zone_radius = true;
  this->no_fly_zone_radius = no_fly_zone_radius.data;
}

NoFlyZoneConditionNode::~NoFlyZoneConditionNode(){
}

BaseNode* BaseNode::get(){
  NoFlyZoneConditionNode* node = new NoFlyZoneConditionNode("NoFlyZoneConditionNode");
  return node;
}
