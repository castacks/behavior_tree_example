#include <base/BaseNode.h>
#include <string>
#include "behavior_tree_example/paused_condition.h"


PausedConditionNode::PausedConditionNode(std::string node_name)
  : BaseNode(node_name){
  
}

bool PausedConditionNode::initialize(){
  ros::NodeHandle* nh = get_node_handle();
  ros::NodeHandle* pnh = get_private_node_handle();

  // ********* CONDITION EXAMPLE USING ROS STD_MSGS *********
  // Initialize data needed to handle the "Paused" condition using only ROS std_msgs.
  // The behavior tree contains a condition node called "Paused" so it will be expecting
  // a boolean published to a topic. The topic name is the condition node's label to lowercase,
  // with spaces replaced by underscores, and _success appended to the end, "/in_no_fly_zone_success".
  // The behavior tree has a "timeout" parameter. If a message is not recieved within the number
  // of seconds specified by "timeout", the condition will be considered in the failure state,
  // so you must publish it at a rate higher than 1/timeout Hz.
  paused_pub = nh->advertise<std_msgs::Bool>("/paused_success", 10);

    // ********* CONDITION EXAMPLE USING THE HELPER CLASS *********
  // Initialize data needed to handle the "paused" condition using the 
  // Condition helper class from the behavior_tree package.
  // Initialize the Condition object with the node handle, and the label on the Condition node
  // in the behavior tree, in this case "paused". The Condtion class automatically
  // sets up a publisher to the /paused_success topic. You can set the Condition class
  // to success by calling in_fly_zone_condition->set(true) or failure by calling
  // in_fly_zone_condition->set(false). You can query the condition's bool using
  // in_fly_zone_condition->get(). Publish the condition by calling in_fly_zone_condition->publish().
  // Examples of these function calls are in the execute() function.
  // As in the method above using only ROS std msgs, you must call the Condition variable's publish
  // function fast enough so that the behavior tree doesn't consider it timed out.
  // paused_condition = new bt::Condition(*nh, "In Fly Zone");
  
  
  // init subscribers
  paused_sub = nh->subscribe("/cmd_vel", 10,
					 &PausedConditionNode::paused_callback,
					 this);
  
  // init publishers
  paused_pub = nh->advertise<std_msgs::Bool>("/paused_success", 10);
  
  // initialization
  bool paused_ = false;
  
  return true;
}

bool PausedConditionNode::execute(){
  std_msgs::Bool paused_msg;
  paused_msg.data = paused_;
  paused_pub.publish(paused_msg);
  // paused_condition->set(!paused_);
  // paused_condition->publish();
  return true;
}


void PausedConditionNode::paused_callback(geometry_msgs::Twist cmd_vel){
  if (cmd_vel.linear.x > 0) paused_ = false;
  else paused_ = true;
}


PausedConditionNode::~PausedConditionNode(){
}

BaseNode* BaseNode::get(){
  PausedConditionNode* node = new PausedConditionNode("PausedConditionNode");
  return node;
}
