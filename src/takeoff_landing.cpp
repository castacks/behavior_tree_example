#include <base/BaseNode.h>
#include <string>
#include "behavior_tree_example/takeoff_landing.h"


TakeoffLandingNode::TakeoffLandingNode(std::string node_name)
  : BaseNode(node_name){
  
}

bool TakeoffLandingNode::initialize(){
  ros::NodeHandle* nh = get_node_handle();
  ros::NodeHandle* pnh = get_private_node_handle();

  // ********* CONDITION EXAMPLE WITHOUT THE HELPER CLASS *********
  // Initialize data needed to handle the "At Flight Altitude" condition using only ROS std_msgs.
  // The behavior tree contains a condition node called "At Flight Altitude" so it will be expecting
  // a boolean published to a topic. The topic name is the condition node's label to lowercase,
  // with spaces replaced by underscores, and _success appended to the end, "/at_flight_altitude_success".
  // The behavior tree has a "timeout" parameter. If a message is not recieved within the number
  // of seconds specified by "timeout", the condition will be considered in the failure state,
  // so you must publish it at a rate higher than 1/timeout Hz.
  at_flight_altitude_pub = nh->advertise<std_msgs::Bool>("/at_flight_altitude_success", 10);

  // ********* CONDITION EXAMPLE USING THE HELPER CLASS *********
  // Initialize data needed to handle the "On Ground" condition using the 
  // Condition helper class from the behavior_tree package.
  // Initialize the Condition object with the node handle, and the label on the Condition node
  // in the behavior tree, in this case "On Ground". The Condtion class automatically
  // sets up a publisher to the /on_ground_success topic. You can set the Condition class
  // to success by calling on_ground_condition->set(true) or failure by calling
  // on_ground_condition->set(false). You can query the condition's bool using
  // on_ground_condition->get(). Publish the condition by calling on_ground_condition->publish().
  // Examples of these function calls are in the execute() function.
  // As in the method above using only ROS std msgs, you must call the Condition variable's publish
  // function fast enough so that the behavior tree doesn't consider it timed out.
  on_ground_condition = new bt::Condition("On Ground");
  

  // ********* ACTION EXAMPLE WITHOUT THE HELPER CLASS *********
  // Initialize data needed to handle the "Takeoff" action by directly publishing and subscribing to topics.
  // We need to subscribe to a std_msgs::Bool topic that tells us whether or not
  // the action is active. Since the label on the actio node in the behavior tree is "Takeoff",
  // the behavior tree will publish the Bool message to the "/takeoff_active" topic.
  // We also need to publish a std_msgs::String topic that
  // tells the bevahior tree the status of the action while it is active.
  // Nothing needs to be published while the action is not active.
  // The string should be "SUCCESS", "RUNNING", or "FAILURE". The behavior tree expects
  // the String status message to be published to the "/takeoff_status" topic.
  // As in the condition publisher, we need to make sure the status gets published
  // at a high enough rate so that it isn't considered a timeout by the behavior tree.
  takeoff_active_sub = nh->subscribe("/takeoff_active", 10,   // Initialize the subscriber
				     &TakeoffLandingNode::takeoff_active_callback, this);
  takeoff_active = false; // This will be used to store the value of the received message.
  // Initialize the publisher that publishes the action's status while it is active.
  takeoff_status_pub = nh->advertise<behavior_tree_msgs::Status>("/takeoff_status", 10);

  // ********* ACTION EXAMPLE USING THE HELPER CLASS *********
  // Initialize data needed to handle the "Land" action using the 
  // Action helper class from the behavior_tree package.
  // The action class needs a node handle as the first parameter and
  // the label of the action node in the behavior tree's config file, in this case "Land".
  land_action = new bt::Action("Land");
  
  // init subscribers
  robot_odom_sub = nh->subscribe("/odometry", 10, &TakeoffLandingNode::robot_odom_callback, this);
  

  // init publishers
  vel_cmd_pub = nh->advertise<geometry_msgs::TwistStamped>("/vel_cmd", 10);
  
  // initialization
  got_robot_odom = false;
  flight_z = 0.99;
  ground_z = 0.01;
  
  return true;
}

bool TakeoffLandingNode::execute(){
  if(!got_robot_odom)
    return true;

  // ********* IN AIR CONDITION EXAMPLE USING ROS STD_MSGS *********
  // The condition should be success (true) if the robot is above the flying height's z level,
  // and failure (failure) otherwise.
  bool at_flight_altitude = robot_odom.pose.pose.position.z > flight_z;
  // Publish the condition as a std_msgs::Bool.
  std_msgs::Bool at_flight_altitude_msg;
  at_flight_altitude_msg.data = at_flight_altitude;
  at_flight_altitude_pub.publish(at_flight_altitude_msg);

  // ********* ON GROUND CONDITION EXAMPLE USING THE HELPER CLASS *********
  // Set the condition to success if the robot is below the ground's z level,
  // or to failure if the robot is above the ground's z level.
  on_ground_condition->set(robot_odom.pose.pose.position.z < ground_z);
  // Publish the condition.
  on_ground_condition->publish();
  
  // ********* TAKEOFF ACTION EXAMPLE WITHOUT THE HELPER CLASS *********
  if(takeoff_active){ // Make sure the robot only takes off if the takeoff action is active.
    behavior_tree_msgs::Status takeoff_status;
    geometry_msgs::TwistStamped vel_cmd;
    vel_cmd.header.stamp = ros::Time::now();
    vel_cmd.header.frame_id = "world";
    
    if(at_flight_altitude){
      // Set the status to "SUCCESS" if the robot has reached the desired altitude.
      takeoff_status.status = behavior_tree_msgs::Status::SUCCESS;
      // Command zero velocity when the robot is done taking off.
      vel_cmd.twist.linear.x = vel_cmd.twist.linear.y = vel_cmd.twist.linear.z = 0;
    }
    else{
      // Set the status to "RUNNING" while the take off is in progress.
      takeoff_status.status = behavior_tree_msgs::Status::RUNNING;
      // Command upward velocity to take off.
      vel_cmd.twist.linear.z = 0.3;
    }
    
    vel_cmd_pub.publish(vel_cmd);

    // Publish the takeoff action's status as a behavior_tree_msgs::Status.
    takeoff_status_pub.publish(takeoff_status);
  }

  // ********* LAND ACTION EXAMPLE USING THE HELPER CLASS *********
  if(land_action->is_active()){ // Make sure the robot only lands if the land action is active.
    geometry_msgs::TwistStamped vel_cmd;
    vel_cmd.header.stamp = ros::Time::now();
    vel_cmd.header.frame_id = "world";
    
    if(on_ground_condition->get()){
      // Set the status to success if the robot has landed.
      land_action->set_success();
      // Command zero velocity when the robot is done landing.
      vel_cmd.twist.linear.x = vel_cmd.twist.linear.y = vel_cmd.twist.linear.z = 0;
    }
    else{
      // Set the status to running while the robot is landing.
      land_action->set_running();
      // Command downward velocity to land.
      vel_cmd.twist.linear.z = -0.3;
    }
    
    vel_cmd_pub.publish(vel_cmd);

    // Publish the land action's status.
    land_action->publish();
  }
  
  return true;
}


void TakeoffLandingNode::takeoff_active_callback(std_msgs::Bool msg){
  takeoff_active = msg.data;
}

void TakeoffLandingNode::robot_odom_callback(nav_msgs::Odometry robot_odom){
  got_robot_odom = true;
  this->robot_odom = robot_odom;
}

TakeoffLandingNode::~TakeoffLandingNode(){
}

BaseNode* BaseNode::get(){
  TakeoffLandingNode* node = new TakeoffLandingNode("TakeoffLandingNode");
  return node;
}
