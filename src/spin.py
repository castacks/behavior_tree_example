#!/usr/bin/python
import rospy
from std_msgs.msg import Bool
from geometry_msgs.msg import TwistStamped
from behavior_tree_msgs.msg import Status

is_active = False

def active_callback(msg):
    global is_active
    is_active = msg.data

prev_is_active = False
    
def timer_callback(msg):
    global prev_is_active

    # spin if active
    if is_active:
        status = Status()
        status.status = Status.RUNNING
        status_pub.publish(status)

        twist = TwistStamped()
        twist.twist.angular.z = 1.0
        ang_vel_pub.publish(twist)

    # send one message right after this action becomes inactive to make the robot stop spinning
    if prev_is_active and not is_active:
        twist = TwistStamped()
        twist.twist.angular.z = 0.0
        ang_vel_pub.publish(twist)

    prev_is_active = is_active

if __name__ == '__main__':
    rospy.init_node('spin')

    active_sub = rospy.Subscriber('/spin_active', Bool, active_callback)
    timer = rospy.Timer(rospy.Duration(0.1), timer_callback)

    status_pub = rospy.Publisher('/spin_status', Status, queue_size=10)
    ang_vel_pub = rospy.Publisher('/ang_vel_cmd', TwistStamped, queue_size=10)

    rospy.spin()
    
