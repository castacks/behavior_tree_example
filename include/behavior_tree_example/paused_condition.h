#ifndef _PAUSED_CONDITION_H_
#define _PAUSED_CONDITION_H_

#include <base/BaseNode.h>
#include <string>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Bool.h>
#include <behavior_tree/behavior_tree.h> // Include the behavior tree library containing the Action and Condition classes.

class PausedConditionNode : public BaseNode {
private:
  
  // ********* CONDTION VARIABLES *********
  // variables for the "In No Fly Zone" condition using only ROS std_msgs
  ros::Publisher paused_pub;
  // variables for the "In Fly Zone" condtion using the behavior tree's Condtion helper class.
  bt::Condition* paused_condition;

  // variable
  bool paused_;
  
  // subscribers
  ros::Subscriber paused_sub;

  // callbacks
  void paused_callback(geometry_msgs::Twist paused);
  
public:
  PausedConditionNode(std::string node_name);
  
  virtual bool initialize();
  virtual bool execute();
  virtual ~PausedConditionNode();

};


#endif
