#ifndef _TRAVEL_H_
#define _TRAVEL_H_

#include <base/BaseNode.h>
#include <string>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Float32.h>
#include <visualization_msgs/Marker.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
#include <behavior_tree_msgs/Status.h> // Include the Status message from behavior_tree_msgs for interacting with the behavior tree without the helper classes.
#include <behavior_tree/behavior_tree.h> // Include the behavior tree library containing the Action and Condition classes.

class TravelNode : public BaseNode {
private:

  bool got_robot_odom, got_home, got_destination;
  nav_msgs::Odometry robot_odom;
  geometry_msgs::PointStamped home, destination;
  float acceptance_radius;
  
  // ********* CONDTION VARIABLES *********
  // variables for the "At Home" condition using only ROS std_msgs
  bool at_home;
  ros::Publisher at_home_pub;
  // variables for the "Visited Destination" condtion using the behavior tree's Condtion helper class.
  bt::Condition* visited_destination_condition;
  
  // ********* ACTION VARIABLES *********
  // variables for the "Go Home" action using only ROS std_msgs
  bool go_home_active;
  ros::Subscriber go_home_active_sub;
  void go_home_active_callback(std_msgs::Bool msg);
  ros::Publisher go_home_status_pub;
  // variables for the "Go To Destination" action using the behavior tree's Action helper class.
  bt::Action* go_to_destination_action;
  
  // subscribers
  ros::Subscriber robot_odom_sub, home_sub, destination_sub;
  
  // publishers
  ros::Publisher vel_cmd_pub;

  // callbacks
  void robot_odom_callback(nav_msgs::Odometry robot_odom);
  void home_callback(geometry_msgs::PointStamped home);
  void destination_callback(geometry_msgs::PointStamped destination);
  
  float distance(nav_msgs::Odometry odom, geometry_msgs::PointStamped point);
  
public:
  TravelNode(std::string node_name);
  
  virtual bool initialize();
  virtual bool execute();
  virtual ~TravelNode();

};


#endif
