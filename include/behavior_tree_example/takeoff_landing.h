#ifndef _TAKEOFF_LANDING_H_
#define _TAKEOFF_LANDING_H_

#include <base/BaseNode.h>
#include <string>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Float32.h>
#include <visualization_msgs/Marker.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
#include <behavior_tree_msgs/Status.h> // Include the Status message from behavior_tree_msgs for interacting with the behavior tree without the helper classes.
#include <behavior_tree/behavior_tree.h> // Include the behavior tree library containing the Action and Condition classes.

class TakeoffLandingNode : public BaseNode {
private:

  bool got_robot_odom;
  nav_msgs::Odometry robot_odom;
  float flight_z, ground_z;
  
  // ********* CONDTION VARIABLES *********
  // variables for the "At Flight Altitude" condition using only ROS std_msgs
  ros::Publisher at_flight_altitude_pub;
  // variables for the "On Ground" condtion using the behavior tree's Condtion helper class.
  bt::Condition* on_ground_condition;

  // ********* ACTION VARIABLES *********
  // variables for the "Takeoff" action using only ROS std_msgs
  ros::Subscriber takeoff_active_sub;
  void takeoff_active_callback(std_msgs::Bool msg);
  bool takeoff_active;
  ros::Publisher takeoff_status_pub;
  // variables for the "Land" action using the behavior tree's Action helper class.
  bt::Action* land_action;
  
  // subscribers
  ros::Subscriber robot_odom_sub;
  
  // publishers
  ros::Publisher vel_cmd_pub;
  
  // callbacks
  void robot_odom_callback(nav_msgs::Odometry robot_odom);
  
public:
  TakeoffLandingNode(std::string node_name);
  virtual bool initialize();
  virtual bool execute();
  virtual ~TakeoffLandingNode();

};


#endif
