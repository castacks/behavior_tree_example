#ifndef _NO_FLY_ZONE_CONDITION_H_
#define _NO_FLY_ZONE_CONDITION_H_

#include <base/BaseNode.h>
#include <string>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Float32.h>
#include <visualization_msgs/Marker.h>
#include <std_msgs/Bool.h>
#include <behavior_tree/behavior_tree.h> // Include the behavior tree library containing the Action and Condition classes.

class NoFlyZoneConditionNode : public BaseNode {
private:
  
  // ********* CONDTION VARIABLES *********
  // variables for the "In No Fly Zone" condition using only ROS std_msgs
  ros::Publisher in_no_fly_zone_pub;
  // variables for the "In Fly Zone" condtion using the behavior tree's Condtion helper class.
  bt::Condition* in_fly_zone_condition;
  
  bool got_robot_odom, got_no_fly_zone, got_no_fly_zone_radius;
  nav_msgs::Odometry robot_odom;
  geometry_msgs::PointStamped no_fly_zone;
  float no_fly_zone_radius;
  
  // subscribers
  ros::Subscriber robot_odom_sub, no_fly_zone_sub, no_fly_zone_radius_sub;
  
  // callbacks
  void robot_odom_callback(nav_msgs::Odometry robot_odom);
  void no_fly_zone_callback(geometry_msgs::PointStamped no_fly_zone);
  void no_fly_zone_radius_callback(std_msgs::Float32 no_fly_zone_radius);
  
public:
  NoFlyZoneConditionNode(std::string node_name);
  
  virtual bool initialize();
  virtual bool execute();
  virtual ~NoFlyZoneConditionNode();

};


#endif
