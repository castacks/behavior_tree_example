#ifndef _ROBOT_NODE_H_
#define _ROBOT_NODE_H_

#include <base/BaseNode.h>
#include <string>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Float32.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

class RobotNode : public BaseNode {
private:

  nav_msgs::Odometry odom;
  geometry_msgs::TwistStamped vel_cmd, ang_vel_cmd;
  ros::Time last_execute_time;
  geometry_msgs::PointStamped home, destination, no_fly_zone;
  std_msgs::Float32 no_fly_zone_radius;
  visualization_msgs::Marker no_fly_zone_marker, no_fly_zone_label, home_label, destination_label;
  visualization_msgs::MarkerArray marker_array;
  
  // subscribers
  ros::Subscriber vel_cmd_sub, ang_vel_cmd_sub;

  // publishers
  ros::Publisher robot_odom_pub, home_pub, destination_pub, no_fly_zone_pub, no_fly_zone_radius_pub, no_fly_zone_vis_pub;

  // callbacks
  void vel_cmd_callback(geometry_msgs::TwistStamped);
  void ang_vel_cmd_callback(geometry_msgs::TwistStamped);
  
public:
  RobotNode(std::string node_name);
  
  virtual bool initialize();
  virtual bool execute();
  virtual ~RobotNode();

};


#endif
